﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CustomerList.aspx.cs" Inherits="Default" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <section>
		<article>
			<p><asp:Label ID="lblPlease" runat="server" Text="Please select a customer:" Width="200px"></asp:Label>
                <asp:DropDownList ID="ddlCustomerList" runat="server" DataSourceID="SqlCustomerData" DataTextField="Name" DataValueField="Name" AutoPostBack="True" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlCustomerData" runat="server" ConnectionString="<%$ ConnectionStrings:CustomerConnect %>" ProviderName="<%$ ConnectionStrings:CustomerConnect.ProviderName %>" SelectCommand="SELECT [CustomerID], [Name], [Address], [State], [City], [ZipCode], [Phone], [Email] FROM [Customer] ORDER BY [Name]"></asp:SqlDataSource>
            </p>
            
            <p>
                <label class="label">Name:</label>
                <asp:Label ID="lblName" runat="server" ReadOnly="True" CssClass="label"></asp:Label>
                <br/>
                <label class="label">Address:</label>
                <asp:Label ID="lblAddress" runat="server" ReadOnly="True" CssClass="label"></asp:Label>
                <br/>
                <label class="label">City:</label>
                <asp:Label ID="lblCity" runat="server" ReadOnly="True" CssClass="label"></asp:Label>
                <br/>
                <label class="label">State:</label>
                <asp:Label ID="lblState" runat="server" ReadOnly="True" CssClass="label"></asp:Label>
                <br/>
                <label class="label">Zip Code:</label>
                <asp:Label ID="lblZipCode" runat="server" ReadOnly="True" CssClass="label"></asp:Label>
                <br/>
                <label class="label">Phone #:</label>
                <asp:Label ID="lblPhone" runat="server" ReadOnly="True" CssClass="label"></asp:Label>
                <br/>
                <label class="label">Email:</label>
                <asp:Label ID="lblEmail" runat="server" ReadOnly="True" CssClass="label"></asp:Label>
                <br/>
                <label class="label">Customer ID:</label>
                <asp:Label ID="lblCustomerID" runat="server" ReadOnly="True" CssClass="label"></asp:Label>

                <br/>
                <br/>
                <asp:Button ID="btnViewContacts" runat="server" Text="View Contact List" OnClick="btnViewContacts_Click" CssClass="button" />
                <br/>
                <asp:Button ID="btnAddToContacts" runat="server" OnClick="btnAddToContacts_Click" CssClass="button"/>
                <br/>
                </p>
                <p>
                <asp:Label ID="lblSystemMessages" runat="server" ForeColor="#00CC00"></asp:Label>
                <br/>
                <asp:Button ID="btnHome" runat="server" OnClick="btnHome_Click" Text="&lt;--- Back to Homepage" CssClass="button" />
                </p>
		</article>
	</section>
</asp:Content>
