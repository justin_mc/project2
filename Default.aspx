﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <section>
        <article>
            <p class ="centered">
                <label>Welcome.</label>
                <br/>
                <br/>
                <asp:Button ID="btnGoCustomerList" runat="server" Text="Go to Customer List -&gt;" OnClick="btnGoCustomerList_Click" CssClass="button" />
                <br/>
                <asp:Button ID="btnGoFeedback" runat="server" Text="Go to Feedback -&gt;" OnClick="btnGoFeedback_Click" CssClass="button" />
            </p>
        </article>
    </section>
</asp:Content>