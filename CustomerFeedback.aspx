﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="CustomerFeedback.aspx.cs" Inherits="CustomerFeedback" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <section>
        <article>
            <p>
                <label class="label">Customer ID:</label>
                <asp:TextBox ID="txtCustomerID" runat="server" Height="22px"></asp:TextBox>
                <asp:Button ID="btnCustomerID" runat="server" Text="Submit" OnClick="btnCustomerID_Click" />
                <asp:RequiredFieldValidator ID="rfvCustomerID" runat="server" ControlToValidate="txtCustomerID" ErrorMessage="This field is required." ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:SqlDataSource ID="sdsCustomerFeedback" runat="server" ConnectionString="<%$ ConnectionStrings:CustomerConnect %>" ProviderName="<%$ ConnectionStrings:CustomerConnect.ProviderName %>" SelectCommand="SELECT CustomerID, Title, DateClosed, SoftwareID FROM Feedback WHERE (CustomerID = ?) ORDER BY DateClosed">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtCustomerID" Name="CustomerID" PropertyName="Text" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </p>
            <p>
                <asp:ListBox ID="lstClosedFeedback" runat="server" CssClass="textbox" DataSourceID="sdsCustomerFeedback" DataTextField="SoftwareID" DataValueField="CustomerID" OnSelectedIndexChanged="lstClosedFeedback_SelectedIndexChanged"></asp:ListBox>
                <asp:RequiredFieldValidator ID="rfvListbox" runat="server" ControlToValidate="lstClosedFeedback" Enabled="False" ErrorMessage="Please select a feedback item" ForeColor="Red"></asp:RequiredFieldValidator>
            </p>
            <p>
                <label class="label">Service Time:</label>
                <asp:RadioButton ID="rbnServiceSatisfied" runat="server" GroupName="service" Text="Satisfied" Enabled="False" CssClass="radio" />
                <asp:RadioButton ID="rbnServiceNeither" runat="server" GroupName="service" Text="Neither" Enabled="False" CssClass="radio" Checked="True" />
                <asp:RadioButton ID="rbnServiceDissatisfied" runat="server" GroupName="service" Text="Dissatisfied" Enabled="False" CssClass="radio" />
                
                <br/>
                <label class="label">Technical Efficiency:</label>
                <asp:RadioButton ID="rbnEfficiencySatisfied" runat="server" GroupName="efficiency" Text="Satisfied" Enabled="False" CssClass="radio" />
                <asp:RadioButton ID="rbnEfficiencyNeither" runat="server" GroupName="efficiency" Text="Neither" Enabled="False" CssClass="radio" Checked="True" />
                <asp:RadioButton ID="rbnEfficiencyDissatisfied" runat="server" GroupName="efficiency" Text="Dissatisfied" Enabled="False" CssClass="radio" />
                <br/>
                <label class="label">Problem Resolution:</label>
                <asp:RadioButton ID="rbnResolutionSatisfied" runat="server" GroupName="resolution" Text="Satisfied" Enabled="False" CssClass="radio" />
                <asp:RadioButton ID="rbnResolutionNeither" runat="server" GroupName="resolution" Text="Neither" Enabled="False" CssClass="radio" Checked="True" />
                <asp:RadioButton ID="rbnResolutionDissatisfied" runat="server" GroupName="resolution" Text="Dissatisfied" Enabled="False" CssClass="radio" />

            </p>
            <p>
                <label class="label">Additional Comments:</label>
                <br/>
                <asp:TextBox ID="txtComments" runat="server" Enabled="False" CssClass="textbox" TextMode="MultiLine"></asp:TextBox>

            </p>
            <p>
                <asp:CheckBox ID="chbxContacted" runat="server" Enabled="False" Text="Yes, I wish to be contacted: " />
                <asp:RadioButton ID="rbnEmail" runat="server" Enabled="False" GroupName="contact" Text="by Email" CssClass="radio" />
                <asp:RadioButton ID="rbnPhone" runat="server" Enabled="False" GroupName="contact" Text="by Phone" CssClass="radio" />

            </p>
            <p>
                <asp:Button ID="btnSubmitAll" runat="server" Enabled="False" OnClick="btnSubmitAll_Click" Text="Finish and Submit" CssClass="button" />

            </p>
        </article>
    </section>
</asp:Content>
