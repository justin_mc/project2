﻿using System;
using System.Data;
using System.Diagnostics;
using System.Web.UI;

/// <summary>
/// The list of customers
/// </summary>
/// <author>
/// Justin McConnell
/// </author>
/// <version>
/// 3/4/2015
/// </version>
public partial class Default : Page
{
    private Customer _selectedCustomer;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            this.ddlCustomerList.DataBind();
        }

        this.ChangeCustomerInfo();
    }

    /// <summary>
    /// Gets the selected customer from the data source.
    /// </summary>
    /// <returns></returns>
    private Customer GetSelectedCustomer()
    {
        DataView customerTable = (DataView) this.SqlCustomerData.Select(DataSourceSelectArguments.Empty);
        Debug.Assert(customerTable != null, "customerTable != null");
        DataRowView row = customerTable[this.ddlCustomerList.SelectedIndex];

        Customer currentCustomer = new Customer();
        currentCustomer.SetName(row["Name"].ToString());
        currentCustomer.SetAddress(row["Address"].ToString());
        currentCustomer.SetCity(row["City"].ToString());
        currentCustomer.SetState(row["State"].ToString());
        currentCustomer.SetZip(row["ZipCode"].ToString());
        currentCustomer.SetPhone(row["Phone"].ToString());
        currentCustomer.SetEmail(row["Email"].ToString());
        currentCustomer.SetCustomerId(row["CustomerID"].ToString());

        return currentCustomer;
    }

    /// <summary>
    /// Changes the customer information to the ones set in the selected customer.
    /// </summary>
    private void ChangeCustomerInfo()
    {
        this._selectedCustomer = this.GetSelectedCustomer();
        this.lblName.Text = this._selectedCustomer.GetName();
        this.lblAddress.Text = this._selectedCustomer.GetAddress();
        this.lblCity.Text = this._selectedCustomer.GetCity();
        this.lblState.Text = this._selectedCustomer.GetState();
        this.lblZipCode.Text = this._selectedCustomer.GetZip();
        this.lblPhone.Text = this._selectedCustomer.GetPhone();
        this.lblEmail.Text = this._selectedCustomer.GetEmail();
        this.lblCustomerID.Text = this._selectedCustomer.GetCustomerId();
        this.btnAddToContacts.Text = "Add " + this._selectedCustomer.GetName() + " To Contacts";
    }
    /// <summary>
    /// Handles the SelectedIndexChanged event of the ddlCustomerList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.ChangeCustomerInfo();
        this.lblSystemMessages.Text = "";
    }
    protected void btnViewContacts_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContactList.aspx");
    }
    /// <summary>
    /// Adds the currently selected customer to the contacts list.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnAddToContacts_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            String name = this._selectedCustomer.GetName();
            CustomerList list = CustomerList.GetList();
            ListItem listItem = list[name];
            if (listItem == null)
            {
                list.AddItem(new ListItem(this._selectedCustomer));
                this.lblSystemMessages.Text = name + " was added to the contacts list.";
            }
            else
            {
                this.lblSystemMessages.Text = name + " was already added to the contacts list.";
            }
        }
        Response.Redirect("ContactList.aspx");
    }
    /// <summary>
    /// Handles the Click event of the btnHome control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnHome_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}