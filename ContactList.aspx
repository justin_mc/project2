﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ContactList.aspx.cs" Inherits="ContactList" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
        <section>
            <article>
                <p>
                    <asp:ListBox ID="lstbxContacts" runat="server" OnSelectedIndexChanged="lstbxContacts_SelectedIndexChanged"></asp:ListBox>
                </p>
                <p>
                    <asp:Button ID="btnSelectAdditionalCustomers" runat="server" OnClick="btnSelectAdditionalCustomers_Click" Text="Select Additional Customers" CssClass="button" />
                    <asp:Button ID="btnRemoveCustomer" runat="server" OnClick="btnRemoveCustomer_Click" Text="Remove Customer" CssClass="button" />
                    <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear" CssClass="button" />
                </p>
                <p>
                    <asp:Label ID="lblMessages" runat="server" ForeColor="Red"></asp:Label>
                </p>
            </article>
        </section>
</asp:Content>

