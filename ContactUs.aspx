﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <section>
        <p>
            <label class="message">Get in touch with us.</label>
        </p>
        <p>
            <label class="label">Phone #:</label><label class="label">88-Ballgame</label>
            <br/>
            <label class="label">Hours:</label><label class="label">M-F 9am-5pm</label>
            <br/>
            <label class="label">Email:</label><label class="label"><asp:HyperLink ID="hplEmail" CssClass="mailto" runat="server">info@ballgame.com</asp:HyperLink></label>
            <br/>
            <label class="label">Address:</label><label class="label">1601 Maple Street</label>
        </p>

    </section>
    
</asp:Content>

